# -*- coding: utf-8 -*
#============================#
# creact by clare 20171215
# used python 2.7
#============================#

class HtmlOutputer(object):

    def __init__(self):
        self.datas = []


    def collect_data(self, data):
        if data is None:
            return
        self.datas.append(data)


    def output_html(self):
        fout = open('output.html', 'w')

        fout.write("<html>") 
        fout.write("<body>")
        fout.write("<table>")  

        for data in self.datas:
            fout.write("<tr>")
            fout.write("<tb>%s</tb>" % data['url'])
            fout.write("<tb>%s</tb>" % data['title'].encode('utf-8'))
            fout.write("<tb>%s</tb>" % data['summary'].encode('utf-8'))
            fout.write("</tr>")

        fout.write("</html>") 
        fout.write("</body>")
        fout.write("</table>") 

        fout.close()
