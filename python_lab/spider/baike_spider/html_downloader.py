# -*- coding: utf-8 -*
#============================#
# creact by clare 20171215
# used python 2.7
#============================#

# note "urllib2", no "urllab2"
import urllib2

class HtmlDownloader(object):

    def download(self, url):
        if url is None:
            return None
        # note "urllib2", no "urllab2"
        response = urllib2.urlopen(url)

        if response.getcode() != 200:
            return None

        return response.read()




